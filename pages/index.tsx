import type { NextPage } from "next";
import Head from "next/head";
import Balance from "../src/components/Balance";
import Networks from "../src/components/Networks";
import { ToastContainer } from "react-toastify";
import styles from "../styles/Home.module.css";
import "react-toastify/dist/ReactToastify.css";
import { WalletProvider } from "../src/contexts/Wallet";

const Home: NextPage = () => {
  return (
    <div className={`${styles.pageWrapper}`}>
      <div className={`${styles.container} ${styles.card}`}>
        <Head>
          <title>Rift Finance</title>
          <meta
            name="description"
            content="Democratizing Access To Financial Power tools"
          />
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <WalletProvider>
            <Balance />
            <Networks />
          </WalletProvider>
        </main>

        <footer className={styles.footer}></footer>
      </div>
      <ToastContainer position="top-center" autoClose={2000} />
    </div>
  );
};

export default Home;
