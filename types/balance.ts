export interface Network {
    name: string
    id: string
    rate: number
}

export interface balance
{
    balance: number
    amount_deposited: number
    interest_accrued: number
    network: Network
}

export interface Token {
    name: string
    id: string
    ticker: string
}
export interface TokenBalance {
    balance: number
    token: Token
}