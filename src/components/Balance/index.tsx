import React from 'react'
import styles from './Balance.module.css'
import { useWallet } from '../../contexts/Wallet'

export default function Index() {
    const { dispatch, state } = useWallet()
    const [days, setDays] = React.useState(0)
    const handleClick = () => {
      dispatch({ type: 'fastForward', days })
    }
    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value;
      if(value === "") {
        setDays(0);
      } else {
      setDays(Number.parseInt(event.target.value))
      }
    }
    return (
        <section className={`${styles.balanceWrapper} ${styles.section}`}>
            { Object.values(state.tokenBalances).map(balance => (
              <div className={styles.card} key={balance.token.id}>
              <p>{balance.token.ticker} - Balance</p>
              <p>{balance.balance.toFixed(2)} {balance.token.ticker}</p>
            </div>
            ))}
          
            <div className={`${styles.card} ${styles.daysToProgress}`}>
              <div>
              <p>Days to Progress</p>
              </div>
              <div>
              <input value={days} onChange={handleInputChange} />
              <button onClick={handleClick}>Enter</button>
              </div>
            </div>
          </section>
    )
}
