import React from "react";
import styles from "./NetworkCard.module.css";
import { useWallet } from "../../contexts/Wallet";
import NetworkCard from "./NetworkCard";

export default function Index() {
  const wallet = useWallet();
  return (
    <section className={`${styles.networkGrid}`}>
      {Object.values(wallet.state.balances).map((balance) => (
        <NetworkCard key={balance.network.id} balance={balance} />
      ))}
    </section>
  );
}
