import React, { useState } from "react";
import { toast } from 'react-toastify';
import { balance } from "../../../types/balance";
import styles from "./NetworkCard.module.css";
import { useWallet } from "../../contexts/Wallet";

interface Props {
  balance: balance;
}

export default function Index({ balance }: Props): JSX.Element {
  const [tabState, setTabState] = useState("deposit");
  const [amount, setAmount] = useState(0);
  const { network, amount_deposited, interest_accrued } = balance;
  const { dispatch } = useWallet();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    if(value === "") {
      setAmount(0);
    } else {
    setAmount(Number.parseFloat(event.target.value))
    }
  }

  const handleConfirm = () => {
    switch (tabState) {
      case "deposit":
        dispatch({ type: "deposit", amount, network, token: {name: 'USDC', id: 'usdc', ticker: 'USDC'} });
        setAmount(0);
        break;
      case "withdraw":
        dispatch({ type: "withdraw", network });
        setAmount(0);
        break;
      default:
        toast.dismiss();
        toast.error("Unknown tab state");
        setAmount(0);
        break;
    }
  };
  return (
    <div className={`${styles.card} ${styles.networkCard}`}>
      <h3>{network.name}</h3>

      <div className={styles.details}>
        <div>
          <span>Current APY:</span> <span>{network.rate}%</span>
        </div>
        <div>
          <span>Amount Deposited:</span> <span>{amount_deposited} USDC</span>
        </div>
        <div>
          <span>Accrued Interest:</span> <span>{interest_accrued} USDC</span>
        </div>
      </div>

      <div className={styles.action}>
        <div className={styles.tab}>
          <button className={tabState === 'deposit' ? styles.active : ''} onClick={() => setTabState('deposit')}>Deposit</button>
          <button className={tabState === 'withdraw' ? styles.active : ''} onClick={() => setTabState('withdraw')} >Withdraw</button>
        </div>

{
  tabState === 'deposit' && 
        <div className={styles.inputContainer}>
          <span>Amount</span>
          <span>
            <input value={amount} onChange={handleInputChange} /> USDC
          </span>
        </div>
}

        <div>
          <button onClick={handleConfirm}>Confirm</button>
        </div>
      </div>
    </div>
  );
}
