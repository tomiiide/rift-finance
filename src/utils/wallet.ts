export function calculateInterest(principal: number, days: number, rate: number) {
   const amount = principal * Math.pow((1 + rate/100),(days / 365));
   const interest = amount - principal;
   return Number.parseFloat(interest.toFixed(2));
}