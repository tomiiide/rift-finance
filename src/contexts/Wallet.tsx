import React, { useContext } from "react";
import { toast } from 'react-toastify';
import { balance as NetworkBalance, Network, TokenBalance, Token } from "../../types/balance";
import { calculateInterest } from "../utils/wallet";

type Action =
  | { type: "withdraw"; network: Network }
  | { type: "deposit"; amount: number; network: Network, token: Token }
  | { type: "fastForward"; days: number };
type Dispatch = (action: Action) => void;
type State = {
  balance: number;
  balances: { [network: string]: NetworkBalance };
  tokenBalances: {
    [token: string]: TokenBalance }
}

type WalletProviderProps = { children: React.ReactNode };

const WalletStateContext = React.createContext<
  { state: State; dispatch: Dispatch } | undefined
>(undefined);

function walletReducer(state: State, action: Action) {
  switch (action.type) {
    case "withdraw":
      // get the balance of the network
      let networkBalance = state.balances[action.network.id];
      // update the total balance
      let newBalance =
        state.balance +
        networkBalance.amount_deposited +
        networkBalance.interest_accrued;
      // subtract the amount from the balance
      networkBalance.amount_deposited = 0;
      networkBalance.interest_accrued = 0;
      // notify the user
        toast.dismiss();
        toast.success("withdraw successful");
      // update the state
      return {
        ...state,
        balance: newBalance,
      };
    case "deposit":
      let tokenBalance = state.tokenBalances[action.token.id]
      // get the balance of the wallet
      if (tokenBalance.balance < action.amount) {
        toast.dismiss();
        toast.error("Insufficient funds");
        return {
          ...state
        };
        
      } else {
        let networkBalance = state.balances[action.network.id];
        // add the amount to the balance
        networkBalance.amount_deposited += action.amount;
        // update the total balance
        tokenBalance.balance -= action.amount
        // update the total balance
        let newBalance = state.balance - action.amount;
        toast.dismiss();
        toast.success("Deposit successful");
        return {
          ...state,
          balance: newBalance,
        };
        
      }
    case "fastForward":
      for (const network in state.balances) {
        let networkBalance = state.balances[network];
        state.balances[network].interest_accrued += calculateInterest(
          networkBalance.amount_deposited,
          action.days,
          networkBalance.network.rate
        );
      }
      toast.dismiss();
      toast.success(`Fast forward ${action.days} days`);

      return {
        ...state,
      };

    default:
      toast.dismiss();
      toast.error(`Unknown action type`);
      return state;
  }
}

const defaultState: State = {
  balance: 2000,
  balances: {
    compound: {
      balance: 0,
      amount_deposited: 0,
      interest_accrued: 0,
      network: {
        name: "Compound",
        id: "compound",
        rate: 5,
      },
    },
    aave: {
      balance: 0,
      amount_deposited: 0,
      interest_accrued: 0,
      network: {
        name: "Aave",
        id: "aave",
        rate: 3,
      },
    },
    curve: {
      balance: 0,
      amount_deposited: 0,
      interest_accrued: 0,
      network: {
        name: "Curve",
        id: "curve",
        rate: 2.5,
      },
    },
  },
  tokenBalances: {
    usdt: {
      balance: 2000,
      token: {
        name: "USDT",
        id: "usdt",
        ticker: "USDT",
      }
    },
    usdc: {
      balance: 2000,
      token: {
        name: "USDC",
        id: "usdc",
        ticker: "USDC",
      }
    }
  }
};

function WalletProvider({ children }: WalletProviderProps) {
  const [state, dispatch] = React.useReducer(walletReducer, defaultState);
  const value = { state, dispatch };
  return (
    <WalletStateContext.Provider value={value}>
      {children}
    </WalletStateContext.Provider>
  );
}

const useWallet = () => {
  const context = React.useContext(WalletStateContext);
  if (context === undefined) {
    throw new Error("useWallet must be used within a WalletProvider");
  }
  return context;
};

export { WalletProvider, useWallet };
