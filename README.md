## Rift.Finance Web app

## Demo
Visit [https://rift-finance.vercel.app/](rift-finance.vercel.app/) to see a demo.

## Installation

First, clone this repo
```bash
git clone https://gitlab.com/tomiiide/rift-finance.git
```   

then install deps
```bash
npm install
# or
yarn
```


run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

